// Sauvegarde et chargement de la progression
function progress_save(n, path) {
  // Sauvegarde le chemin de la page n.
  localStorage.setItem("page" + n, path);
}
function progress_load(n) {
  // Charge le chemin de la page n (éventuellement `null` si la page n'a pas encore été visitée)
  return localStorage.getItem("page" + n);
}
