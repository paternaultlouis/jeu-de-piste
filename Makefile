all: build

build: $(PDF)
	lektor build --output-path public

serve: $(PDF)
	lektor serve
