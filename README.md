Jeu de piste
============

Ce jeu de piste a pour but d'enseigner aux élèves quelques compétences de base nécessaires à l'utilisation d'un ordinateur de bureau (indépendamment du système d'exploitation).

- [Commencer le jeu](http://paternaultlouis.forge.apps.education.fr/jeu-de-piste/)
- [Plus d'info](http://paternaultlouis.forge.apps.education.fr/jeu-de-piste/apropos)
- [Utiliser le jeu en classe](http://paternaultlouis.forge.apps.education.fr/jeu-de-piste/prof)

Construction du site web
------------------------

Le site web est construit avec [Lektor](https://getlektor.com). Pour le construire :

~~~sh
lektor build --output-path public
~~~

Pour lancer un serveur qui reconstruit le site à la moindre modification :

~~~sh
lektor serve
~~~

Quoi de neuf ?
--------------

Lire le [CHANGELOG](CHANGELOG.md).
