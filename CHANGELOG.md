## 2024-11-28

- La progression est sauvegardée (en local) : il est maintenant possible de revenir en arrière.

## 2024-09

- Correction de scripts pour mieux fonctionner avec Windows.
- Attend que l'utilisateur·ice appuie sur une touche avant de fermer les programmes

## 2024-05

- Migration de Framagit à la Forge des communs numériques éducatifs.

## 2021-08

- Première version utilisable
