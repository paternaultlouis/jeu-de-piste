#!/usr/bin/env python3

################################################################################
#       _                  _              _     _
#      | |                | |            (_)   | |
#      | | ___ _   _    __| | ___   _ __  _ ___| |_ ___
#  _   | |/ _ \ | | |  / _` |/ _ \ | '_ \| / __| __/ _ \
# | |__| |  __/ |_| | | (_| |  __/ | |_) | \__ \ ||  __/
#  \____/ \___|\__,_|  \__,_|\___| | .__/|_|___/\__\___|
#                                  | |
#                                  |_|
#
# Le mot de passe est : jasmin.
#
################################################################################

import base64
import os
import tempfile
import webbrowser

TEMPLATE = """
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>{title}</title>
  </head>
  <body>
    <p>{content}</p>

    <hr>

    <p><a href="http://paternaultlouis.forge.apps.education.fr/jeu-de-piste/jeu/9a6744951cd96d59a4a240d97187e865/">Recommencer.</a></p>
  </body>
</html>
"""

MESSAGE = b"UmF0w6nCoCEgSWwgbmUgZmF1dCBwYXMgZXjDqWN1dGVyIGNlIGZpY2hpZXIsIG1haXMgbCdvdXZyaXIgYXZlYyB1biDDqWRpdGV1ciBkZSBjbGFzc2UuIEVzc2F5ZXogZGUgwqvCoGwnb3V2cmlyIGF2ZWPigKbCoMK7"

if __name__ == "__main__":
    (filedescriptor, filename) = tempfile.mkstemp(suffix=".html", text=True)
    with os.fdopen(filedescriptor, mode="wt", encoding="utf-8") as file:
        file.write(TEMPLATE.format(
        title="Raté…",
        content=base64.b64decode(MESSAGE).decode().strip(),
        ))
    webbrowser.open("file://{}".format(filename))

