#!/usr/bin/env python3

################################################################################
#       _                  _              _     _
#      | |                | |            (_)   | |
#      | | ___ _   _    __| | ___   _ __  _ ___| |_ ___
#  _   | |/ _ \ | | |  / _` |/ _ \ | '_ \| / __| __/ _ \
# | |__| |  __/ |_| | | (_| |  __/ | |_) | \__ \ ||  __/
#  \____/ \___|\__,_|  \__,_|\___| | .__/|_|___/\__\___|
#                                  | |
#                                  |_|
#
# Il ne faut pas afficher ce fichier, mais l'exécuter.
#
################################################################################

import base64
import itertools
import logging
import os
import pathlib
import socket
import sys

MOTDEPASSE = b"Y29xdWVsaWNvdA=="

def home_AURA1():
    """Renvoit le dossier partagé sur le réseau, pour les ordinateurs des lycées de la région AURA. Variante 1."""
    import winreg

    for qualité in ("eleves", "professeurs"):
        maison = pathlib.Path("{}.{}/{}/{}".format(
          winreg.ExpandEnvironmentStrings('%LogonServer%'),
          winreg.ExpandEnvironmentStrings('%UserDNSDomain%'),
          qualité,
          winreg.ExpandEnvironmentStrings('%UserName%'),
          ))
        try:
            if maison.exists():
                yield maison
        except Exception as error:
            logging.error(error)
            continue

def home_AURA2():
    """Renvoit le dossier partagé sur le réseau, pour les ordinateurs des lycées de la région AURA. Variante 2."""
    import win32net
    yield pathlib.Path("//{}/{}/{}".format(
        socket.gethostbyname_ex(win32net.NetGetDCName().strip("\\"))[0],
          "professeurs",
          os.getlogin()
          ))

def home_AURA3():
    """Renvoit le dossier partagé sur le réseau, pour les ordinateurs des lycées de la région AURA. Variante 3."""
    import win32net
    yield pathlib.Path("{}/{}/{}/{}".format(
        win32net.NetGetDCName(),
        socket.getfqdn().partition(".")[2],
        "professeurs",
        os.getlogin(),
        ))

def home_local():
    """Renvoit le dossier personnel de l'utilisateur ou utilisatrice sur l'ordinateur."""
    yield pathlib.Path.home()

def homes(subdir=None):
    for iterator in (home_AURA1, home_AURA2, home_AURA3, home_local):
        try:
            for home in iterator():
                if subdir is None:
                    yield home
                else:
                    yield home / subdir
        except:
            pass

def document_windows():
    # From https://stackoverflow.com/a/3859336
    import ctypes.wintypes
    CSIDL_PERSONAL= 5       # My Documents
    SHGFP_TYPE_CURRENT= 0   # Want current, not default value
    buf= ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(0, CSIDL_PERSONAL, 0, SHGFP_TYPE_CURRENT, buf)
    yield pathlib.Path(buf.value)

def document_home():
    for home in homes():
        for doc in ("Documents", "Mes documents"):
            yield home / doc

def documents(subdir):
    for iterator in (document_home, document_windows):
        try:
            for document in iterator():
                yield document / subdir
        except:
            continue

    
def decode(text):
    return base64.b64decode(text).decode().strip()

if __name__ == "__main__":
    repertoire = pathlib.Path(*pathlib.Path(os.path.abspath(os.path.dirname(__file__))).parts[1:])
    trouvé = False
    for candidat in itertools.chain(homes("SNT"), documents("SNT")):
        if (
            str(repertoire).lower() == str(candidat).lower()
            or
            str(candidat).lower().endswith(str(repertoire).lower()) # Pas tout à fait correct mais je n'arrive pas à faire mieux
            ):
            if trouvé:
                continue
            print("Le mot de passe est", decode(MOTDEPASSE))
            trouvé = True

    if not trouvé:
        print("Le fichier Python devrait être placé dans un dossier SNT dans votre répertoire personnel. Réessayez…")

    input("Appuyez sur [Entrée] pour terminer le programme…")
