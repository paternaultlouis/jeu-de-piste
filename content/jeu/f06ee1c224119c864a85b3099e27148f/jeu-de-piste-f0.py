#!/usr/bin/env python3

################################################################################
#       _                  _              _     _
#      | |                | |            (_)   | |
#      | | ___ _   _    __| | ___   _ __  _ ___| |_ ___
#  _   | |/ _ \ | | |  / _` |/ _ \ | '_ \| / __| __/ _ \
# | |__| |  __/ |_| | | (_| |  __/ | |_) | \__ \ ||  __/
#  \____/ \___|\__,_|  \__,_|\___| | .__/|_|___/\__\___|
#                                  | |
#                                  |_|
#
# Vous avez ouvert ce fichier. Il faut maintenant l'exécuter.
#
################################################################################

import base64
import webbrowser

ADRESSE = b"aHR0cHM6Ly9wYXRlcm5hdWx0bG91aXMuZm9yZ2UuYXBwcy5lZHVjYXRpb24uZnIvamV1LWRlLXBpc3RlL2pldS9mMDZlZTFjMjI0MTE5Yzg2NGE4NWIzMDk5ZTI3MTQ4Zi8/c2VjcmV0PWNyb2N1cw=="

if __name__ == "__main__":
    suivant = base64.b64decode(ADRESSE).decode().strip()
    print("Votre navigateur web devrait ouvrir une page pour continuer le jeu.")
    print("Si cela ne fonctionne pas, veuillez suivre ce lien:")
    print(suivant)
    webbrowser.open(suivant)

